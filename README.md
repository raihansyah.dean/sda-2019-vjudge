# SDA 2019 VJudge

Repo ini adalah repo yang dibuat untuk kumpulan solusi soal-soal latihan di vjudge. Harap menggunakan repo ini semestinya dan sebisa mungkin mencoba mengerjakan soal-soal terlebih dahulu sebelum melihat pembahasan ataupun solusinya. 

Repo ini akan terdiri dari 2 bagian yaitu kumpulan ide pengerjaan soal dan solusi soal dalam bentuk Java. Diharapkan solusi dan ide hanya digunakan ketika sudah sulit mencari solusi.

**Selamat mengerjakan :)**

## Pembahasan Ide Soal

Bagian ini akan berisi kumpulan ide untuk mengerjakan setiap soal yang ada pada masing-masing kontes. Ide akan berisi ide awal dan juga beberapa hint untuk mengerjakan soal-soal tersebut. Diharapkan hint-hint digunakan satu demi persatu agar melatih proses pembelajaran kalian.

## Solusi Soal

Bagian ini berisi solusi dari seluruh soal yang ada di kontes-kontes vjudge dalam bentuk file Java. Perlu diingat bahwa solusi-solusi yang ada bukan merupakan solusi satu-satunya.

## Tim Pembuat
- Raihansyah Attallah Andrian
- Rafif Taris
- Gusti Ngurah Yama Adi Putra
