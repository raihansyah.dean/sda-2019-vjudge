# SDA 2019 VJudge - Asbtract Data Type

Soal-soal yang ada pada kategori ini adalah soal-soal yang kurang lebih se-level dengan WS1. Soal-soal berikut menguatkan pada materi `Abstract Data Type`.

Materi yang perlu diperhatikan adalah penggunaan Abstract Data Type seperti `Stack`, `Queue`, `Array`, `ArrayList`, dan `Dequeue`.

## Daftar Soal

1. Jodoh Yang Tertukar - Hard+
2. Main Bentengan - Medium+
3. A Rey - Easy
4. Rental Kaset - Medium
5. Tepok Kartu Pokemon - Medium+
6. Dilarang Jomblo - Hard+
7. Biro Jodoh - Medium
8. Mainin Benang - Medium+
9. UKOR Tenis Meja - Hard
10. Bikun LOONA - Easy
