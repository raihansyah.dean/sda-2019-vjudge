# SDA 2019 VJudge - Introduction

Soal-soal yang ada pada kategori ini adalah soal-soal yang kurang lebih se-level dengan WS1. Soal-soal berikut menguatkan pada materi `implementasi kode` dan `kompleksitas`.

Materi yang perlu diperhatikan adalah perhitungan kompleksitas waktu (Big-Oh) dan materi-materi dasar pada DDP2.

## Daftar Soal

1. Mario Kabur - Easy
2. Kuda Lumping - Hard+
3. COMPFEST 12 - Medium+
4. Tebak Huruf - Easy+
5. Karnaugh Map - Hard
6. Anna (Gram) Ou - Hard
7. Perfectly Balanced - Medium
8. MatDas 1 - Easy
9. Judi - Medium
